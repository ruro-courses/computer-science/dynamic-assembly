#Directories {
SRC = src
BIN = bin
DOC = doc
LIB = lib
FUN = fun
DIRS = $(BIN) $(LIB)
# }
#Help { 
ifndef SPEC_FILE
.DEFAULT:
	echo -e "$$(cat $(DOC)/.help)"
	read var; var=$(DOC)/$$var*; for i in $$var; do if [ -f $$i ]; then echo -e "$$(cat $(DOC)/.sep)"; echo -e "$$(cat $$i)"; fi; done
help: .DEFAULT 
else
# }
#Compatibility mode {
compat: spec
	$(call $(LOG), [32m[LOG in makefile][0m : SPEC_FILE detected, running in compat mode.)
	if [[ -f $(SPEC_FILE) ]]; then cat $(SPEC_FILE) | $(BIN)/spec.o; else\
	$(error SPEC_FILE '$(SPEC_FILE)' doesn't exist)
endif
# }
#Make directories {
.PHONY: $(DIRS)
$(DIRS):
	mkdir -p $@
# }
#Files { 
SOURCES =               $(wildcard $(SRC)/*)
BIN_SHORT =             $(patsubst $(SRC)/%.c, %,$(filter $(SRC)/%.c, $(SOURCES)))
BIN_OBJ =               $(patsubst %, $(BIN)/%.o, $(BIN_SHORT))
BIN_RUN =               $(patsubst %, run_%, $(BIN_SHORT))
FUNCTIONS =             $(wildcard $(FUN)/*)
FUN_LIBS_SHORT =        $(patsubst $(FUN)/%, %, $(FUNCTIONS))
FUN_LIBS_ASM =          $(patsubst %, $(LIB)/%.asm, $(FUN_LIBS_SHORT))
FUN_LIBS_OBJ =          $(patsubst %, $(LIB)/%.o,   $(FUN_LIBS_SHORT))
FUN_LIBS_SHARED =       $(patsubst %, $(LIB)/%.so,  $(FUN_LIBS_SHORT))
FUN_LIBS_PRIME_SHORT =  $(patsubst $(FUN)/%, %_prime, $(FUNCTIONS))
FUN_LIBS_PRIME_ASM =    $(patsubst %, $(LIB)/%.asm, $(FUN_LIBS_PRIME_SHORT))
FUN_LIBS_PRIME_OBJ =    $(patsubst %, $(LIB)/%.o,   $(FUN_LIBS_PRIME_SHORT))
FUN_LIBS_PRIME_SHARED = $(patsubst %, $(LIB)/%.so,  $(FUN_LIBS_PRIME_SHORT))
# }
#Parameters {
CPARAMS = -std=gnu99 -m32 -lm -ldl -rdynamic -O3 -Wfatal-errors -Wall -Wextra -Wunreachable-code -Wswitch-default
SHARED = -m32 -fpic -shared
DEFINES = $(foreach TMP,$(filter _D%,$(.VARIABLES)),$(addsuffix =$($(TMP)),$(subst _D,-D,$(TMP))))
# } 
#Verbose output switch {
ifndef _DVERBOSE
_DVERBOSE=M
endif
ifeq ($(_DVERBOSE),H)
$(info [34m[DEBUG in makefile][0m : Running in VERBOSE mode H, makefile commands will be printed, as well as compiler WARNINGS.)
$(info [34m[DEBUG in makefile][0m : Found Sources       : $(SOURCES))
$(info [34m[DEBUG in makefile][0m :    -Binary names    : $(BIN_SHORT))
$(info [34m[DEBUG in makefile][0m :    -Binaries        : $(BIN_OBJ))
$(info [34m[DEBUG in makefile][0m : Found Functions     : $(FUNCTIONS))
$(info [34m[DEBUG in makefile][0m :    -Function names  : $(FUN_LIBS_SHORT))
$(info [34m[DEBUG in makefile][0m :    -Function Objects: $(FUN_LIBS_OBJ))
$(info [34m[DEBUG in makefile][0m :    -Shared Libraries: $(FUN_LIBS_SHARED))
CPARAMS += -Wall -Wextra -Wno-long-long -Wno-variadic-macros -Wformat-security -Wignored-qualifiers -Winit-self -Wswitch-default -Wpointer-arith -Wtype-limits -Wempty-body -Wstrict-prototypes -Wold-style-declaration -Wold-style-definition -Wmissing-parameter-type -Wnested-externs -Wno-pointer-sign -fexceptions
LOG=info
else
ifeq ($(_DVERBOSE),M)
LOG=info 
else
LOG=
endif
.SILENT:
endif
# }
#Force rebuild switch {
ifdef _DREBUILD
$(call $(LOG), [32m[LOG in makefile][0m : User specified compile-time parameters. Rebuilding from scratch.)
.PHONY: $(BIN_OBJ)
ifeq ($(_DREBUILD), 2)
.PHONY: $(FUN_LIBS_SHARED) $(FUN_LIBS_PRIME_SHARED)
endif
endif
# }
#Make {
.SECONDEXPANSION:
.PHONY: $(BIN_SHORT) $(FUN_LIBS_SHORT) $(FUN_LIBS_PRIME_SHORT) $(BIN_RUN)
$(BIN_SHORT):$$(patsubst %, $(BIN)/%.o, $$@)
$(BIN_OBJ): $(SOURCES) | $(BIN)
	$(call $(LOG), [32m[LOG in makefile][0m : Compiling module (with cc)             : $@)
	$(CC) $(patsubst $(BIN)/%.o, $(SRC)/%.c, $@) $(DEFINES) $(CFLAGS) $(CPARAMS) -o $@
	$(call $(LOG), [32m[LOG in makefile][0m : Done.)
$(BIN_RUN):$$(patsubst run_%, $(BIN)/%.o, $$@)
	$(patsubst run_%, $(BIN)/%.o, $@)

$(FUN_LIBS_SHORT):$$(patsubst %, $(LIB)/%.so, $$@)
$(FUN_LIBS_SHARED):$$(patsubst $(LIB)/%.so, $(LIB)/%.o, $$@)
	$(call $(LOG), [32m[LOG in makefile][0m : Making into a shared lib (with cc)     : $@)
	$(CC) $< $(CFLAGS) $(SHARED) -o $@
	$(call $(LOG), [32m[LOG in makefile][0m : Done.)
.INTERMEDIATE: $(FUN_LIBS_OBJ) $(FUN_LIBS_ASM)
$(FUN_LIBS_OBJ):$$(patsubst $(LIB)/%.o, $(LIB)/%.asm, $$@)
	$(call $(LOG), [32m[LOG in makefile][0m : Compiling (with nasm)                  : $@)
	nasm -f elf32 -o $@ $<
$(FUN_LIBS_ASM):$$(patsubst $(LIB)/%.asm, $(FUN)/%, $$@) | $(BIN)/builder.o $(LIB) 
	$(call $(LOG), [32m[LOG in makefile][0m : Making assembler listing (with builder): $@)
	echo "$$(cat $<)" | $(BIN)/builder.o > $@

$(FUN_LIBS_PRIME_SHORT):$$(patsubst %, $(LIB)/%.so, $$@)
$(FUN_LIBS_PRIME_SHARED):$$(patsubst $(LIB)/%.so, $(LIB)/%.o, $$@)
	$(call $(LOG), [32m[LOG in makefile][0m : Making into a shared lib (with cc)     : $@)
	$(CC) $< $(CFLAGS) $(SHARED) -o $@
	$(call $(LOG), [32m[LOG in makefile][0m : Done.)
.INTERMEDIATE: $(FUN_LIBS_PRIME_OBJ) $(FUN_LIBS_PRIME_ASM)
$(FUN_LIBS_PRIME_OBJ):$$(patsubst $(LIB)/%.o, $(LIB)/%.asm, $$@)
	$(call $(LOG), [32m[LOG in makefile][0m : Compiling (with nasm)                  : $@)
	nasm -f elf32 -o $@ $<
$(FUN_LIBS_PRIME_ASM):$$(patsubst $(LIB)/%_prime.asm, $(FUN)/%, $$@) | $(BIN)/builder.o $(LIB) 
	$(call $(LOG), [32m[LOG in makefile][0m : Making assembler listing (with builder): $@)
	echo "$$(cat $<)'" | $(BIN)/builder.o > $@
# }
#Misc {
.SUFFIXES:
.PHONY: help all clean all_bin all_lib clean_bin clean_lib
all: all_bin all_lib
all_bin: $(BIN_OBJ)
all_lib: $(FUN_LIBS_SHARED) $(FUN_LIBS_PRIME_SHARED)
clean: clean_bin clean_lib
clean_bin:
	$(call $(LOG), [32m[LOG in makefile][0m : Removing module binaries.)
	rm -rf $(BIN)
clean_lib:
	$(call $(LOG), [32m[LOG in makefile][0m : Removing libraries.)
	rm -rf $(LIB)
	rm -rf $(FUN)/f1 $(FUN)/f2 $(FUN)/f3
# }
# vim:foldmethod=marker:foldenable:foldlevel=0:foldmarker={,}:foldnestmax=2:noai:nocin:nosi:inde=<CR>
