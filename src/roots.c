/*
 	[roots.c]
 	Accepts a pair of function library names and two floating point values
	and searches for the intersection of them somewhere in the specified range.
*/

/*INCLUDE {*/
#include <math.h>
#include <unistd.h>

#include "loader.h"
#include "logger.h"
/*INCLUDE }*/

/*DEFINE {*/
#ifndef EPSILON
	#define EPSILON 0.001
#endif

#ifndef TRY_HARDNESS
	#define TRY_HARDNESS 16
#endif

#ifndef ROOT_METHOD
	#define ROOT_METHOD 0
#endif

#ifndef MAX_ITER
	#define MAX_ITER 65536
#endif

#ifndef USE_FUNCTIONAL_DERIVATIVES
	#define USE_FUNCTIONAL_DERIVATIVES 1
#endif

#define F(x) (f1(x)-f2(x))
#define P(x) (p1(x)-p2(x))
#define sgnCmp(a,b) ((a > 0) == (b > 0))
/*DEFINE }*/

/*FUNCTION DECLARATION {*/
int newton_step(int *, double *, double, double, func, func, func, func);
int bisect_step(int *, double *, double *, func, func);
int main(void);
/*FUNCTION DECLARATION }*/

/*FUNCTION IMPLEMENTATION {*/
int newton_step(int *iter, double *x, double left, double right,
                func f1, func p1, func f2, func p2) {
	double delta;
	(*iter)++;
	if (*x <= left || right <= *x || *iter >= MAX_ITER)
		return 1;
	if (!sgnCmp(F(*x - EPSILON / 2), F(*x + EPSILON / 2)))
		return -1;
	if (fabs(F(*x)) <= EPSILON * EPSILON)
		return -1;
	if (!p1 || !p2)
		delta = F(*x) / ((F(*x + EPSILON / 2) - F(*x - EPSILON / 2)) / EPSILON);
	else
		delta = F(*x) / P(*x);
	*x -= delta;
	return 0;
}

int bisect_step(int *iter, double *left, double *right,
                func f1, func f2) {
	double l, m, r;
	(*iter)++;
	if (*iter >= MAX_ITER)
		return -1;
	l = F(*left);
	r = F(*right);
	m = F((*left + *right) / 2);
	if (l == 0) {
		*right = *left;
		return -1;
	}
	if (r == 0) {
		*left = *right;
		return -1;
	}
	
	if (sgnCmp(l, r))
		return 1;
	if (fabs(*left - *right) < EPSILON / 2) {
		m = (*left + *right) / 2;
		*left = m;
		*right = m;
		return -1;
	}
	if (!sgnCmp(l, m))
		*right = (*left + *right) / 2;
	if (!sgnCmp(m, r))
		*left = (*left + *right) / 2;
	return 0;
}

int main(void) {
	func f1 = NULL, p1 = NULL;
	func f2 = NULL, p2 = NULL;
	double left, right, x, step;
	int iter = 0, tries = 0;
	if (isatty(fileno(stdin)))
		fprintf(stderr, "%s", "Please, input the range to search: ");
	if (scanf(" %lf %lf", &left, &right) != 2)
		_error("Incorrect range format.\n");
	if (right < left) {
		x = right;
		right = left;
		left = x;
	}
	switch (ROOT_METHOD) {
		case 0: {	/*TRY HARD*/
			if (USE_FUNCTIONAL_DERIVATIVES) {
				loadFunctionAndPrime(&f1, &p1);
				loadFunctionAndPrime(&f2, &p2);
			}
			else {
				loadFunction(&f1);
				loadFunction(&f2);
			}
			if (sgnCmp(F(left), F(right))) {
				_log("Function has same signs on ends of specified range, starting newton's method.\n");
				step = left;
				while (step <= right) {
					step += (right - left) / TRY_HARDNESS;
					x = step;
					iter = 0;
					while (!newton_step(&iter, &x, left, right, f1, p1, f2, p2));
					if (fabs(F(x)) >= sqrt(EPSILON))
						_log("Failed finding the intersection with starting point x = %lf .\n", step);
					else {
						_log("Found intersection at x = %lf in %d/"STRINGIFY(TRY_HARDNESS)\
						     " tries and %d iterations.\n", x, tries, iter);
						if (!isatty(fileno(stdin)))
							printf("%lf\n", x);
						return 0;
					}
					tries++;
				}
				_error("Failed to find the root even after using brute force "
				       STRINGIFY(TRY_HARDNESS)" times. Either root doesn't exist"
				       "or function has bad convergence.\n");
			}
			_log("Function has opposite signs on ends of specified range, starting the bisection method.\n");
			}	/*TRY HARD*/ /*FALLTHRU*/
		case 1: {	/*BISECTION ONLY {*/
			if (!f1)
				loadFunction(&f1);
			if (!f2)
				loadFunction(&f2);
			while (!bisect_step(&iter, &left, &right, f1, f2));
			if (fabs(left - right) >= EPSILON)
				_error("Failed to find the intersection point using bisection. Last range is [%lf;%lf]\n",
				       left, right);
			_log("Found intersection at x = %lf in %d iterations.\n", left, iter);
			if (!isatty(fileno(stdin)))
				printf("%lf\n", left);
			return 0;
			}		/*BISECTION ONLY }*/
		case 2: {	/*NEWTON ONLY {*/
			if (!USE_FUNCTIONAL_DERIVATIVES)
				_error("Non-Functional derivatives currently don't work in newton's method and will be deprecated.\n");
			loadFunctionAndPrime(&f1, &p1);
			loadFunctionAndPrime(&f2, &p2);
			x = (left + right) / 2;
			while (!newton_step(&iter, &x, left, right, f1, p1, f2, p2));
			if (fabs(F(x)) >= sqrt(EPSILON))
				_error("Failed to find the intersection point using only lazy newton's method. Last point is x = %lf\n",
				       x);
			_log("Found intersection at x = %lf in %d iterations,\n", x, iter);
			if (!isatty(fileno(stdin)))
				printf("%lf\n", x);
			return 0;
			}		/*NEWTON ONLY }*/
		default:
			_error("Root search method '%s' is not defined.\n", STRINGIFY(ROOT_METHOD));
	}
}
/*FUNCTON IMPLEMENTATION }*/

// vim:foldmethod=marker:foldenable:foldlevel=0:foldmarker={*,}*:foldnestmax=2
