/*
    [optimize.h]
    Provides everything required for optimization of functional node trees.
*/

/*INCLUDE {*/
#ifndef _OPTIMIZE_H
#define _OPTIMIZE_H

#include <math.h>

#include "logger.h"
#include "builder.h"
/*INCLUDE }*/

/*MACRO DEFENITION {*/
#define _build_case_M(a)		case(a):		return (constant)M##a;
#define _build_case_F(a, val)	case(F##a):		return (constant)val;
#define changeOrder(p)			(p)->type ^= _BCH
#define max(a, b)				((a)>(b)?(a):(b))
#define inRange(a, b)			(fabs(a-b) <= EPSILON/2)
#define snap(a, b)				if (inRange(a,valueOfStd(b))) {head->type = VSTD; head->std_value = b; return;}
/*MACRO DEFENITION }*/

/*FUNCTION DECLARATION {*/
constant FUN_compute(node_type, constant);
constant BOP_compute(node_type, constant, constant);
node *STK_isolate(node *);
constant valueOf(node *);
constant valueOfStd(std_constant);
void VAL_optimize(node *);
void FUN_optimize(node *, node *);
void BOP_optimize(node *, node *, node *);
/*FUNCTION DECLARATION }*/

/*FUNCTION IMPLEMENTATION {*/
constant FUN_compute(node_type type, constant argument) {
	switch (type) {
		case FSIN:
			return (constant)sin((double)argument);
		case FCOS:
			return (constant)cos((double)argument);
		case FTAN:
			return (constant)tan((double)argument);
		case FCTG:
			return 1 / ((constant)tan((double)argument));
		default:
			errId("function node", type);
			return (constant)0;
	}
}

constant BOP_compute(node_type type, constant left, constant right) {
	switch (type) {
		case BADD:
			return left + right;
		case BSUB:
			return left - right;
		case BRSB:
			return right - left;
		case BMUL:
			return left * right;
		case BDIV:
			return left / right;
		case BRDV:
			return right / left;
		default:
			errId("operator node", type);
			return (constant)0;
	}
}

node *STK_isolate(node *head) {
	_error("Function doesn't fit into the `x87 co-processor` stack."\
		   "(This is a known bug)\n");
	node *separator = malloc(sizeof(node));
	if (!separator)
		errMem();
		
	separator->type = _STK;
	separator->argument = head;
	return separator;
}

constant valueOf(node *head) {
	if (!head)
		errHd();
	if (isVariable(head))
		_error("A variable was assumed to be a consant.\n");
	if (isConstant(head))
		return head->value;
	if (isStdConstant(head))
		return valueOfStd(head->std_value);
	errId("constant node", head->type);
	return (constant)0;
}

constant valueOfStd(std_constant value) {
	switch (value) {
			_build_case_F(__1, 1);
			_build_case_F(__Z, 0);
			_build_case_F(L2T, M_LOG10E);
			_build_case_F(L2E, M_LOG2E);
			_build_case_F(_PI, M_PI);
			_build_case_F(LG2, M_LN2 / M_LN10);
			_build_case_F(LN2, M_LN2);
			_build_case_M(_E);
			_build_case_M(_PI_2);
			_build_case_M(_PI_4);
			_build_case_M(_1_PI);
			_build_case_M(_2_PI);
			_build_case_M(_2_SQRTPI);
			_build_case_M(_SQRT2);
			_build_case_M(_SQRT1_2);
		default:
			errId("standart constant", value);
			return (constant)0;
	}
}

void VAL_optimize(node *head) {
	if (!head)
		errHd();
		
	if (isStdConstant(head) || isVariable(head)) {
		head->height = 1;
		return;
	}
	
	if (isConstant(head)) {
		constant value = head->value;
		snap(value, F__1);
		snap(value, F__Z);
		snap(value, FL2T);
		snap(value, FL2E);
		snap(value, F_PI);
		snap(value, FLG2);
		snap(value, FLN2);
		snap(value, _E);
		snap(value, _PI_2);
		snap(value, _PI_4);
		snap(value, _1_PI);
		snap(value, _2_PI);
		snap(value, _2_SQRTPI);
		snap(value, _SQRT2);
		snap(value, _SQRT1_2);
		head->height = 1;
		return;
	}
	errId("value node", head->type);
}

void FUN_optimize(node *head, node *argument) {
	if (!head || !argument)
		errHd();
		
	if (!isFunction(head))
		_error("Non-FUN node assumed to be FUN. This shouldn't happen check for memory leaks/overflows.\n");
		       
	if (isConstant(argument) || isStdConstant(argument)) {
		head->value = FUN_compute(head->type, valueOf(argument));
		head->type = VCON;
		VAL_optimize(head);
		return;
	}
	head->argument = argument;
	head->height = argument->height;
}

void BOP_optimize(node *head, node *left, node *right) {
	node *tmp;
	if (!head || !left || !right)
		errHd();
		
	if (!isOperator(head))
		_error("Non-BOP node assumed to be BOP. This shouldn't happen check for memory leaks/overflows.\n");
		       
	if ((isConstant(left) || isStdConstant(left)) &&
	    (isConstant(right) || isStdConstant(right))) {
		head->value = BOP_compute(head->type, valueOf(left), valueOf(right));
		head->type = VCON;
		VAL_optimize(head);
		return;
	}
	if (left->height > MAX_X87_DEPTH)
		left = STK_isolate(left);
		
	if (right->height > MAX_X87_DEPTH)
		right = STK_isolate(right);
		
	if (left->height < right->height) {
		tmp = left;
		left = right;
		right = tmp;
		if (isOrdered(head))
			changeOrder(head);
	}
	head->left = left;
	head->right = right;
	head->height = max(left->height, right->height + 1);
}
/*FUNCTION IMPLEMENTATION }*/

/*INCLUDE GUARD {*/
#endif
/*INCLUDE GUARD }*/

// vim:foldmethod=marker:foldenable:foldlevel=0:foldmarker={*,}*:foldnestmax=2
