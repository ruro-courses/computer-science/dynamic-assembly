/*
 	[tester.c]
 	Accepts a shared function library name on stdin,
	Looks for `lib/<libname>.so` and loads the embedded
	function from this library. Then, while stdin contains valid floats,
 	Prints `f(<value>) = <result>` to stdin.
*/

/*INCLUDE {*/
#include <float.h>

#include "loader.h"
/*INCLUDE }*/

/*FUNCTION DECLARATION {*/
int main(void);
/*FUNCTION DECLARATION }*/

/*FUNCTION IMPLEMENTATION {*/
int main(void) {
	double x;
	func f;
	loadFunction(&f);
	if (isatty(fileno(stdin)))
		fprintf(stderr, "%s", "Please input x: ");
	while (scanf("%le", &x) == 1) {
		if (isatty(fileno(stdin)))
			fprintf(stderr, "f(%.*le) = %.*le | Please input x: ", DECIMAL_DIG, x,
			        DECIMAL_DIG, f(x));
		else
			printf("%lf\n", f(x));
	}
	return 0;
}
/*FUNCTION IMPLEMENTATION }*/

// vim:foldmethod=marker:foldenable:foldlevel=0:foldmarker={*,}*:foldnestmax=2
