/*
 	[builder.c]
 	Accepts function defenition on stdin,
 	Prints assembler listing of the function on stdout,
 	Reports errors/debug messages on stderr.
*/

/*INCLUDE {*/
#include <stdio.h>
#include <stdlib.h>

#include "builder.h"
#include "logger.h"
#include "assembler.h"
#include "optimize.h"
/*INCLUDE }*/

/*MACRO DEFENITION {*/
#define getCh(a)				(fscanf(stdin, "%c", &a) == 1)
#define getFl(a)				(fscanf(stdin, "%le", &a) == 1)
#define setVal(i, t)			nodes[i].value = t
#define init(i, t)				nodes[i++].type = t
/*MACRO DEFENITION }*/

/*FUNCTION DECLARATION {*/
node *build(node **);
node *differentiate(node *);
int main(void);
/*FUNCTION DECLARATION }*/

/*FUNCTION IMPLEMENTATION {*/
node *build(node **ptr_reader) {
	node *head = NULL, *t1 = NULL, *t2 = NULL;
	if (!ptr_reader || !(head = *ptr_reader))
		errHd();
	if (isBreaker(head))
		errBrk();
		
	(*ptr_reader)--;
	if (isOperator(head)) {
		t1 = build(ptr_reader);
		t2 = build(ptr_reader);
		BOP_optimize(head, t1, t2);
		return head;
	}
	if (isFunction(head)) {
		t1 = build(ptr_reader);
		FUN_optimize(head, t1);
		return head;
	}
	if (isValue(head)) {
		VAL_optimize(head);
		return head;
	}
	errId("node", head->type);
	return NULL;
}

node *differentiate(node *head) {
	node *head_prime = NULL, *t1 = NULL, *t2 = NULL, *t3 = NULL, *t4 = NULL,
	      *t5 = NULL;
	if (!(head_prime = malloc(sizeof(node))))
		errMem();
		
	if (!head)
		errHd();
	if (isBreaker(head))
		errBrk();
	
	if (isOperator(head)) {
		if (isLinear(head)) {
			head_prime->type = head->type;
			head_prime->left = differentiate(head->left);
			head_prime->right = differentiate(head->right);
			BOP_optimize(head_prime, head_prime->left, head_prime->right);
			return head_prime;
		}
		if (isCompound(head)) {
			if (!(t1 = malloc(sizeof(node))) || !(t2 = malloc(sizeof(node)))
			    || !(t3 = malloc(sizeof(node))) || !(t4 = malloc(sizeof(node))))
				errMem();
				
			t1->type = BMUL;
			BOP_optimize(t1, differentiate(head->left), head->right);
			t2->type = BMUL;
			BOP_optimize(t2, head->left, differentiate(head->right));
			if (head->type == BMUL) {
				head_prime->type = BADD;
				BOP_optimize(head_prime, t1, t2);
				return head_prime;
			}
			t5 = head->right;
			if (head->type == BRDV) {
				t5 = t1;
				t1 = t2;
				t2 = t5;
				t5 = head->left;
			}
			if (head->type == BDIV || head->type == BRDV) {
				t3->type = BSUB;
				BOP_optimize(t3, t1, t2);
				t4->type = BMUL;
				BOP_optimize(t4, t5, t5);
				head_prime->type = BDIV;
				BOP_optimize(head_prime, t3, t4);
				return head_prime;
			}
		}
		errId("operator node", head->type);
	}
	if (isFunction(head)) {
		if (!(t1 = malloc(sizeof(node))) || !(t2 = malloc(sizeof(node)))
		    || !(t3 = malloc(sizeof(node))) || !(t4 = malloc(sizeof(node)))
		    || !(t5 = malloc(sizeof(node))))
			errMem();
			
		switch (head->type) {
			case FSIN:
				t1->type = FCOS;
				FUN_optimize(t1, head->argument);
				break;
			case FCOS:
				t1->type = BSUB;
				t2->type = FSIN;
				t3->type = VSTD;
				t3->std_value = F__Z;
				FUN_optimize(t2, head->argument);
				BOP_optimize(t1, t3, t2);
				break;
			case FTAN:
				t1->type = BDIV;
				t2->type = VSTD;
				t2->std_value = F__1;
				t3->type = FCOS;
				t4->type = BMUL;
				FUN_optimize(t3, head->argument);
				BOP_optimize(t4, t3, t3);
				BOP_optimize(t1, t2, t4);
				break;
			case FCTG:
				t1->type = BDIV;
				t2->type = VCON;
				t2->value = (constant) - 1.0;
				t3->type = FSIN;
				t4->type = BMUL;
				FUN_optimize(t3, head->argument);
				BOP_optimize(t4, t3, t3);
				BOP_optimize(t1, t2, t4);
				break;
			default:
				errId("function node", head->type);
		}
		head_prime->type = BMUL;
		BOP_optimize(head_prime, t1, differentiate(head->argument));
		return head_prime;
	}
	if (isValue(head)) {
		head_prime->type = VSTD;
		if (isConstant(head) || isStdConstant(head)) {
			head_prime->std_value = F__Z;
			return head_prime;
		}
		if (isVariable(head)) {
			head_prime->std_value = F__1;
			return head_prime;
		}
		errId("value node", head->type);
	}
	errId("node", head->type);
	return NULL;
}

int main(void) {
	node *function;
	node nodes[MAX_SPEC_SIZE];
	unsigned i = 0;
	char c = '$';
	constant tmp = 0.0f;
	init(i, _BRK);
	
	while (i < MAX_SPEC_SIZE) {
		if (!getCh(c)) {
			_error("Unexpected EOF. Possibly a faulty Reverse Polish Notation of function. Stopping.\n");
			return 0;
		}
		switch (c) {
			case '+':
				init(i, BADD);
				break;
			case '-':
				init(i, BSUB);
				break;
			case '*':
				init(i, BMUL);
				break;
			case '/':
				init(i, BDIV);
				break;
			case 'e':
			case 'E':
				nodes[i].std_value = _E;
				init(i, VSTD);
				break;
			case 'p':
			case 'P':
				if (getCh(c))
					if (c == 'i' || c == 'I') {
						nodes[i].std_value = F_PI;
						init(i, VSTD);
						break;
					}
				errCh();
				break;
			case 's':
			case 'S':
				if (getCh(c))
					if (c == 'i' || c == 'I')
						if (getCh(c))
							if (c == 'n' || c == 'N') {
								init(i, FSIN);
								break;
							}
				errCh();
				break;
			case 'c':
			case 'C':
				if (getCh(c)) {
					if (c == 'o' || c == 'O')
						if (getCh(c)) {
							if (c == 's' || c == 'S') {
								init(i, FCOS);
								break;
							}
							if (c == 't' || c == 'T') {
								init(i, FCTG);
								break;
							}
						}
					if (c == 't' || c == 'T')
						if (getCh(c))
							if (c == 'g' || c == 'G') {
								init(i, FCTG);
								break;
							}
				}
				errCh();
				break;
			case 't':
			case 'T':
				if (getCh(c)) {
					if (c == 'g' || c == 'G') {
						init(i, FTAN);
						break;
					}
					if (c == 'a' || c == 'A')
						if (getCh(c))
							if (c == 'n' || c == 'N') {
								init(i, FTAN);
								break;
							}
				}
				errCh();
				break;
			case 'x':
				init(i, VLDX);
				break;
			case ' ':
				break;
			case '\n':
			case '$':
			case '\'':
				function = &nodes[i - 1];
				_debug("Building function...\n");
				function = build(&function);
				if (c == '\'') {
					_debug("Calculating the derivative...\n");
					function = differentiate(function);
				}
				_debug("Generating assembler listing...\n");
				asmgen(function);
				return 0;
				init(i, _BRK);
				break;
			default:
				if (c >= '0' && c <= '9') {
					ungetc(c, stdin);
					if (getFl(tmp)) {
						setVal(i, tmp);
						init(i, VCON);
						break;
					}
					_error("Found an unexpected digit '%c' in node (id %d).\n", c, i);
				}
				errCh();
		}
	}
	_error("Not enough memory. Consider increasing MAX_SPEC_SIZE.\n");
}
/*FUNCTION IMPLEMENTATION }*/

// vim:foldmethod=marker:foldenable:foldlevel=0:foldmarker={*,}*:foldnestmax=2
