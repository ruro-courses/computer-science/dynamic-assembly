/*
    [logger.h]
 	Provides everything required for logging of errors/debug messages.
*/

/*INCLUDE {*/
#ifndef _LOGGER_H
#define _LOGGER_H

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
/*INCLUDE }*/

/*DEFINE {*/
#ifndef VERBOSE
	#define VERBOSE		M
#endif
#define DEB_PREFIX		"\x1b[34m[DEBUG"
#define LOG_PREFIX		"\x1b[32m[LOG"
#define ERR_PREFIX		"\x1b[31m[ERROR"

#define STRINGIFY(a)	_STRINGIFY(a)
#define _STRINGIFY(a)	#a

#define errCh()			_error("Unknown symbol in function defenition at node %d. Last read char is \"%c\" (hex: %X).\n", i, c, c)
#define errHd()			_error("Head lost. This shouldn't happen check for memory leaks/overflows.\n");
#define errBrk()		_error("Unexpected breaker reached. Possibly a faulty Reverse Polish Notation of function.\n");
#define errId(a, b)		_error("Unknown "a" type (id 0x%08x).\n", b);
#define errMem()    	_error("Failed allocating memory.\n");

#define _debug(a, ...)	_print(2, a, ""__FILE__, __LINE__, __func__, ##__VA_ARGS__)
#define _log(a, ...)	_print(1, a, ""__FILE__, __LINE__, __func__, ##__VA_ARGS__)
#define _error(a, ...)	_print(0, a, ""__FILE__, __LINE__, __func__, ##__VA_ARGS__)
#define _pre(a...)		fprintf(stderr, "%s in %s:%s line %d][0m : ", a)
/*DEFINE }*/

/*FUNCTION DECLARATION {*/
void _print(unsigned level, const char *message, const char *file,
            unsigned line, const char *function, ...) __attribute__((format(printf, 2, 6)));
/*FUNCTION DECLARATION }*/

/*FUNCTION IMPLEMENTATION {*/
void _print(unsigned level, const char *message, const char *file,
            unsigned line, const char *function, ...) {
	va_list args;
	va_start(args, function);
	switch (STRINGIFY(VERBOSE)[0]) {
		case 'H':
			if (level == 2) {
				_pre(DEB_PREFIX, file, function, line);
				vfprintf(stderr, message, args);
			}/*FALLTHRU*/
		case 'M':
			if (level == 1) {
				_pre(LOG_PREFIX, file, function, line);
				vfprintf(stderr, message, args);
			}/*FALLTHRU*/
		case 'L':
			if (level == 0) {
				_pre(ERR_PREFIX, file, function, line);
				vfprintf(stderr, message, args);
				exit(1);
			}/*FALLTHRU*/
		case 'N':
			va_end(args);
			return;
		default:
			break;
	}
	_pre(ERR_PREFIX, ""__FILE__, __func__, __LINE__);
	fprintf(stderr, "Invalid VERBOSE logging level. - %s", STRINGIFY(VERBOSE));
	exit(-1);
}
/*FUNCTION IMPLEMENTATION }*/

/*INCLUDE GUARD {*/
#endif
/*INCLUDE GUARD }*/

// vim:foldmethod=marker:foldenable:foldlevel=0:foldmarker={*,}*:foldnestmax=2
