/*
 	[assembler.h]
 	Provides everything required for work with assembler listings.
*/

/*INCLUDE {*/
#ifndef _ASSEMBLER_H
#define _ASSEMBLER_H

#include <math.h>
#include <string.h>

#include "builder.h"
#include "logger.h"
/*INCLUDE }*/

/*CONST DEFINE {*/
#define ASM_PREAMBLE	("section .bss\n"\
                         "buffer: resb 8\n"\
                         "section .text\n"\
                         "global function\n"\
                         "function:\n")
#define ASM_EPILOGUE	("xor eax,eax\n"\
                         "ret\n\n")

#define ASM_STK_SAVE	("add esp, 108\n"\
                         "fnsave [esp]\n"\
                         "finit\n")
#define ASM_STK_LOAD	("fstp qword[buffer]\n"\
                         "frstor [esp]\n"\
                         "sub esp, 108\n"\
                         "fld qword[buffer]\n")

#define ASM_LOADX		("fld qword[esp+4]\n")
#define ASM_LOAD		("mov dword[buffer  ], 0x%08x\n"\
                         "mov dword[buffer+4], 0x%08x\n"\
                         "fld qword[buffer]\n")

#define ASM_F__1		("fld1\n")
#define ASM_F__Z		("fldz\n")
#define ASM_FL2T		("fldl2t\n")
#define ASM_FL2E		("fldl2e\n")
#define ASM_F_PI		("fldpi\n")
#define ASM_FLG2		("fldlg2\n")
#define ASM_FLN2		("fldln2\n")

#define ASM_FSIN		("fsin\n")
#define ASM_FCOS		("fcos\n")
#define ASM_FTAN		("fptan\n"\
                         "fstp st0\n")
#define ASM_FCTG		("fptan\n"\
                         "fdivrp\n")

#define ASM_BADD		("faddp\n")
#define ASM_BMUL		("fmulp\n")
#define ASM_BSUB		("fsubp\n")
#define ASM_BDIV		("fdivp\n")
#define ASM_BRSB		("fsubrp\n")
#define ASM_BRDV		("fdivrp\n")
/*CONST DEFINE }*/

/*MACRO DEFINE {*/
#define _out(a, ...)	fprintf(stdout, a, ##__VA_ARGS__)

#define _asm_case_F(a)\
	case a:\
	_out(ASM_##a);\
	break

#define _asm_case_M(a)\
	case a:\
	VAL_const_asm(M##a);\
	break
/*MACRO DEFINE }*/

/*FUNCTION DECLARATION {*/
void asmgen(node *);
void _asm(node *);
void VAL_asm(node *);
void VAL_const_asm(constant);
void VAL_x87_asm(std_constant);
void VAL_math_asm(std_constant);
void FUN_asm(node_type);
void BOP_asm(node_type);
/*FUNCTION DECLARATION }*/

/*FUNCTION IMPLEMENTATION {*/
void asmgen(node *head) {
	_out(ASM_PREAMBLE);
	_asm(head);
	_out(ASM_EPILOGUE);
}

void _asm(node *head) {
	if (!head)
		errHd();
	if (isBreaker(head))
		errBrk();
		
	if (isValue(head)) {
		VAL_asm(head);
		return;
	}
	if (isFunction(head)) {
		_asm(head->argument);
		FUN_asm(head->type);
		return;
	}
	if (isOperator(head)) {
		_asm(head->left);
		_asm(head->right);
		BOP_asm(head->type);
		return;
	}
	if (isStackSep(head)) {
		_out(ASM_STK_SAVE);
		_asm(head->argument);
		_out(ASM_STK_LOAD);
		return;
	}
	errId("node", head->type);
}

void VAL_asm(node *head) {
	if (isVariable(head)) {
		_out(ASM_LOADX);
		return;
	}
	if (isStdConstant(head)) {
		if (isx87Constant(head)) {
			VAL_x87_asm(head->std_value);
			return;
		}
		if (isMathConstant(head)) {
			VAL_math_asm(head->std_value);
			return;
		}
		errId("constant node", head->std_value);
	}
	if (isConstant(head)) {
		VAL_const_asm(head->value);
		return;
	}
	errId("value node", head->type);
}

void VAL_const_asm(constant value) {
	int data[2];
	memcpy(data, &value, 8);
	_out(ASM_LOAD, data[0], data[1]);
}

void VAL_x87_asm(std_constant id) {
	switch (id) {
			_asm_case_F(F__Z);
			_asm_case_F(F__1);
			_asm_case_F(FL2T);
			_asm_case_F(FL2E);
			_asm_case_F(F_PI);
			_asm_case_F(FLG2);
			_asm_case_F(FLN2);
		default:
			errId("x87 constant", id);
	}
}

void VAL_math_asm(std_constant id) {
	switch (id) {
			_asm_case_M(_E);
			_asm_case_M(_PI_2);
			_asm_case_M(_PI_4);
			_asm_case_M(_1_PI);
			_asm_case_M(_2_PI);
			_asm_case_M(_2_SQRTPI);
			_asm_case_M(_SQRT2);
			_asm_case_M(_SQRT1_2);
		default:
			errId("math.h constant", id);
	}
}

void FUN_asm(node_type type) {
	switch (type) {
			_asm_case_F(FSIN);
			_asm_case_F(FCOS);
			_asm_case_F(FTAN);
			_asm_case_F(FCTG);
		default:
			errId("function node", type);
	}
}

void BOP_asm(node_type type) {
	switch (type) {
			_asm_case_F(BADD);
			_asm_case_F(BMUL);
			_asm_case_F(BSUB);
			_asm_case_F(BDIV);
			_asm_case_F(BRSB);
			_asm_case_F(BRDV);
		default:
			errId("operator node", type);
	}
}
/*FUNCTION IMPLEMENTATION }*/

/*INCLUDE GUARD {*/
#endif
/*INCLUDE GUARD }*/

// vim:foldmethod=marker:foldenable:foldlevel=0:foldmarker={*,}*:foldnestmax=2
