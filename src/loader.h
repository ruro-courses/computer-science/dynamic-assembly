/*
    [loader.h]
 	Provides everything required for dynamic loading of
	functions embedded in shared libraries.
*/

/*INCLUDE {*/
#ifndef _LOADER_H
#define _LOADER_H

#include <stdio.h>
#include <dlfcn.h>
#include <string.h>
#include <unistd.h>

#include "logger.h"
/*INCLUDE }*/

/*DEFINE & MACRO & TYPEDEF {*/
#ifndef MAX_LIB_NAME_SIZE
	#define MAX_LIB_NAME_SIZE 256
#endif

typedef double (*func)(double);
/*DEFINE & MACRO & TYPEDEF  }*/

/*FUNCTION DECLARATION {*/
char *_getName(void);
void loadFunctionByName(func *, char *);
void loadFunction(func *);
void loadFunctionAndPrime(func *, func *);
/*FUNCTION DECLARATION }*/

/*FUNCTION IMPLEMENTATION {*/
char *_getName(void) {
	char *lib;
	if (!(lib = malloc(MAX_LIB_NAME_SIZE + 32)))
		errMem();
	if (isatty(fileno(stdin)))
		fprintf(stderr, "%s", "Please input the name of the shared library: ");
	strcpy(lib, "lib/");
	if (scanf("%"STRINGIFY(MAX_LIB_NAME_SIZE)"s", lib + 4) != 1)
		_error("Failed to read library name.\n");
	return lib;
}

void loadFunctionByName(func *f, char *lib) {
	void *sofile;
	sofile = dlopen(lib, RTLD_NOW);
	if (!sofile)
		_error("Unable to open shared library '%s'.\ndlfcn.h: '%s'\n", lib, dlerror());
	*f = dlsym(sofile, "function");
	if (!*f)
		_error("Unable to load the embedded function.\ndlfcn.h: '%s'\n", dlerror());
}

void loadFunction(func *f) {
	char *lib = _getName();
	strcat(lib, ".so");
	loadFunctionByName(f, lib);
}

void loadFunctionAndPrime(func *f, func *p) {
	char *lib = _getName();
	strcat(lib, ".so");
	loadFunctionByName(f, lib);
	lib[strlen(lib) - 3] = '\0';
	strcat(lib, "_prime.so");
	loadFunctionByName(p, lib);
}
/*FUNCTION IMPLEMENTATION }*/

/*INCLUDE GUARD {*/
#endif
/*INCLUDE GUARD }*/

// vim:foldmethod=marker:foldenable:foldlevel=0:foldmarker={*,}*:foldnestmax=2
