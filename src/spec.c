/*
 	[spec.c]
 	Parses spec files or user input and manipulates other modules to
	calculate the area of a shape, specified by 3 function.
*/


/*INCLUDE {*/
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>

#include "logger.h"
/*INCLUDE }*/

/*DEFINE {*/
#ifndef MAX_LIB_NAME_SIZE
	#define MAX_LIB_NAME_SIZE 256
#endif

#ifndef MAX_SPEC_SIZE
	#define MAX_SPEC_SIZE 1024
#endif
/*DEFINE }*/

/*FUNCTION DECLARATION {*/
void createFunFile(char *, char *);
void make(char *);
double getRoot(char *, char *, double, double);
double getArea(char *, double, double);
int main(void);
/*FUNCTION DECLARATION }*/

/*FUNCTION IMPLEMENTATION {*/
void createFunFile(char *name, char *contents) {
	make("fun");
	char path[MAX_LIB_NAME_SIZE + 8];
	strcpy(path, "fun/");
	strcat(path, name);
	FILE *fp = fopen(path, "w");
	if (!fp)
		_error("Couldn't create file at '%s'.\n", path);
	fputs(contents, fp);
	fclose(fp);
}

void make(char *options) {
	char cmd[32];
	strcpy(cmd, "make ");
	strcat(cmd, options);
	FILE *make = popen(cmd, "w");
	if (!make)
		_error("Couldn't run make.\n");
	if (pclose(make))
		_error("Couldn't make %s.\n", options);
}

double getRoot(char *f1, char *f2, double left, double right) {
	char cmd[2 * MAX_LIB_NAME_SIZE + 128];
	double value;
	strcpy(cmd, "echo \"");
	sprintf(cmd + strlen(cmd), "%lf %lf", left, right);
	strcat(cmd, " ");
	strcat(cmd, f1);
	strcat(cmd, " ");
	strcat(cmd, f2);
	strcat(cmd, "\" | bin/roots.o");
	FILE *root = popen(cmd, "r");
	if (!root)
		_error("Couldn't run execute the roots module.\n");
	if (!(fscanf(root, "%lf", &value) == 1))
		_error("Couldn't read the value from roots module.\n");
	pclose(root);
	return value;
}

double getArea(char *f, double left, double right) {
	char cmd[MAX_LIB_NAME_SIZE + 128];
	double value;
	strcpy(cmd, "echo \"");
	sprintf(cmd + strlen(cmd), "%lf %lf", left, right);
	strcat(cmd, " ");
	strcat(cmd, f);
	strcat(cmd, "\" | bin/integral.o");
	FILE *integral = popen(cmd, "r");
	if (!integral)
		_error("Couldn't run execute the integral module.\n");
	if (!(fscanf(integral, "%lf", &value) == 1))
		_error("Couldn't read the value from integral module.\n");
	pclose(integral);
	return value;
}

int main(void) {
	double left, right;
	char f1[MAX_LIB_NAME_SIZE + 1];
	char f2[MAX_LIB_NAME_SIZE + 1];
	char f3[MAX_LIB_NAME_SIZE + 1];
	if (isatty(fileno(stdin))) {
		_log("Starting interactive mode...\n");
		_log("Please input the first function name: ");
		scanf("%"STRINGIFY(MAX_LIB_NAME_SIZE)"s", f1);
		_log("Please input the second function name: ");
		scanf("%"STRINGIFY(MAX_LIB_NAME_SIZE)"s", f2);
		_log("Please input the third function name: ");
		scanf("%"STRINGIFY(MAX_LIB_NAME_SIZE)"s", f3);
		_log("Please input the range in which to search for the figure: ");
		scanf("%lf %lf", &left, &right);
		make("all_bin");
	}
	else {
		char spec1[MAX_SPEC_SIZE + 1];
		char spec2[MAX_SPEC_SIZE + 1];
		char spec3[MAX_SPEC_SIZE + 1];
		scanf("%lf %lf ", &left, &right);
		scanf("%"STRINGIFY(MAX_SPEC_SIZE)"[^\n]%*c", spec1);
		scanf("%"STRINGIFY(MAX_SPEC_SIZE)"[^\n]%*c", spec2);
		scanf("%"STRINGIFY(MAX_SPEC_SIZE)"s", spec3);
		createFunFile("f1", spec1);
		createFunFile("f2", spec2);
		createFunFile("f3", spec3);
		make("all_bin");
		make("f1 f2 f3");
		make("f1_prime f2_prime f3_prime");
		strcpy(f1, "f1");
		strcpy(f2, "f2");
		strcpy(f3, "f3");
	}
	double f1f2, f2f3, f3f1;
	f1f2 = getRoot(f1, f2, left, right);
	f2f3 = getRoot(f2, f3, left, right);
	f3f1 = getRoot(f3, f1, left, right);
	double S;
	S = getArea(f1, f3f1, f1f2);
	S += getArea(f2, f1f2, f2f3);
	S += getArea(f3, f2f3, f3f1);
	_log("The area of the specified shape is %lf.\n", S);
	return 0;
}
/*FUNCTION IMPLEMENTATION }*/

// vim:foldmethod=marker:foldenable:foldlevel=0:foldmarker={*,}*:foldnestmax=2
