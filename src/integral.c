/*
 	[integral.c]
 	Accepts a shared function library name and two floating point values
	on stdin, looks for `lib/<libname>.so` and loads the embedded function
	from this library. Then, computes the definite integral of the function
	over the specified range.
*/

/*INCLUDE {*/
#include <math.h>

#include "loader.h"
/*INCLUDE }*/

/*DEFINE {*/
#ifndef MIN_ITER
	#define MIN_ITER 4
#endif

#ifndef EPSILON
	#define EPSILON 0.001
#endif
/*DEFINE }*/

/*FUNCTION DECLARATION {*/
int integrate(double *, double, double, func);
int main(void);
/*FUNCTION DECLARATION }*/

/*FUNCTION IMPLEMENTATION {*/
int integrate(double *sum, double left, double right, func f) {
	double old, new, step, x, sgn;
	int iter = 1;
	sgn = 1.0;
	if(left > right)
	{
		x = left;
		left = right;
		right = x;
		sgn = -1.0;
	}
	step = (right - left);
	new = f(left) * step;
	do {
		old = new;
		new = 0;
		x = left + step / 2;
		while (x < right) {
			new += f(x);
			x += step;
		}
		step /= 2;
		new *= step;
		new += old / 2;
	} while (++iter < MIN_ITER || fabs(new - old) > EPSILON / 2);
	*sum = new*sgn;
	return iter;
}

int main(void) {
	func f;
	double left, right, sum;
	int iter;
	if (isatty(fileno(stdin)))
		fprintf(stderr, "%s", "Please, input the range to integrate: ");
	if (scanf(" %lf %lf", &left, &right) != 2)
		_error("Incorrect range format.\n");
	loadFunction(&f);
	iter = integrate(&sum, left, right, f);
	_log("Integral from %lf to %lf is %lf after %d iterations.\n",
	     left, right, sum, iter);
	if (!isatty(fileno(stdin)))
		printf("%lf\n", sum);
	return 0;
}
/*FUNCTION IMPLEMENTATION }*/

// vim:foldmethod=marker:foldenable:foldlevel=0:foldmarker={*,}*:foldnestmax=2
