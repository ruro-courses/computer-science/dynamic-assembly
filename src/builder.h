/*
 	[builder.h]
 	Provides everything required for work with functional trees (nodes).
*/

/*INCLUDE {*/
#ifndef _BUILDER_H
#define _BUILDER_H
/*INCLUDE }*/

/*CONST DEFINE {*/
#ifndef MAX_SPEC_SIZE
	#define MAX_SPEC_SIZE 1024
#endif

#ifndef EPSILON
	#define EPSILON 0.001
#endif

#ifndef MAX_X87_DEPTH
	#define MAX_X87_DEPTH 6
#endif
/*CONST DEFINE }*/

/*MACRO DEFINE {*/
#define isConstant(p)		((p)->type == VCON)
#define isStdConstant(p)	((p)->type == VSTD)
#define isVariable(p)		((p)->type == VLDX)
#define isStackSep(p)		((p)->type == _STK)
#define isValue(p)			((p)->type & _VAL)
#define isFunction(p)		((p)->type & _FUN)
#define isOperator(p)		((p)->type & _BOP)
#define isOrdered(p)		((p)->type & _BRV)
#define isLinear(p)			((p)->type == BADD || (p)->type == BSUB || (p)->type == BRSB)
#define isCompound(p)		((p)->type == BMUL || (p)->type == BDIV || (p)->type == BRDV)
#define isx87Constant(p)	((p)->std_value & _FSTD)
#define isMathConstant(p)	((p)->std_value & _MSTD)
#define isBreaker(p)		(!(p)->type)
/*MACRO DEFINE }*/

/*ENUM TYPEDEF {*/
typedef enum {
	//BREAKER NODE
	_BRK = 0x0000,
	//VALUE (Height = 1, Union = value)
	_VAL = 0x000F,
	VCON = 0x0001,
	VLDX = 0x0002,
	VSTD = 0x0003,
	//FUNCTION (Height = argument->height, Union = argument)
	_FUN = 0x00F0,
	FSIN = 0x0010,
	FCOS = 0x0020,
	FTAN = 0x0030,
	FCTG = 0x0040,
	//BINARY OPERATOR (Height = max(left->height,right->height+1), Union = right,left)
	_BOP = 0x0F00,
	BADD = 0x0100,
	BMUL = 0x0200,
	_BRV = 0x0400,	// ?	3rd bit determines, if operation is ordered
	_BCH = 0x0800,	//?		4th bit shows, if the arguments were swapped
	BSUB = 0x0400,	//0100
	BDIV = 0x0500,	//0101
	BRSB = 0x0C00,	//1100
	BRDV = 0x0D00,	//1101
	//x87 STACK SEPARATOR (Height = 1, Union = argument)
	_STK = 0xF000
} node_type;

typedef enum {
	__UND		= 0x0000,
	//x87 FLD CONSTANTS
	_FSTD		= 0x000F,
	F__Z		= 0x0001,
	F__1		= 0x0002,
	FL2T		= 0x0003,
	FL2E		= 0x0004,
	F_PI		= 0x0005,
	FLG2		= 0x0006,
	FLN2		= 0x0007,
	//math.h CONSTANTS
	_MSTD		= 0x00F0,
	_E			= 0x0010,
	_PI_2		= 0x0020,
	_PI_4		= 0x0030,
	_1_PI		= 0x0040,
	_2_PI		= 0x0050,
	_2_SQRTPI	= 0x0060,
	_SQRT2		= 0x0070,
	_SQRT1_2	= 0x0080
} std_constant;
/*ENUM TYPEDEF }*/

/*NODE TYPEDEF {*/
typedef double constant;

typedef struct node {
	node_type type;
	int height;
	union {
		struct {
			struct node *left;
			struct node *right;
		};
		struct node *argument;
		constant value;
		std_constant std_value;
	};
} node;
/*NODE TYPEDEF }*/

/*INCLUDE GUARD {*/
#endif
/*INCLUDE GUARD }*/

// vim:foldmethod=marker:foldenable:foldlevel=0:foldmarker={*,}*:foldnestmax=2
